/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.classutils.drawlot;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author lendle
 */
public class StudentLoader {

    public List<Student> loadStudents(File csvFile) throws Exception {
        List<Student> students = new ArrayList<>();
        InputStreamReader reader = new InputStreamReader(
                new FileInputStream(csvFile), "utf-8");
        Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(reader);
        for (CSVRecord record : records) {
            Student student = new Student();
            String id = record.get("id");
            String name = record.get("name");
            student.setId(id);
            student.setName(name);
            students.add(student);
            //out.println("<li>"+id+","+name+"</li>");
        }
        reader.close();
        return students;
    }
}
