/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.classutils.rockpaperscissors;

/**
 *
 * @author lendle
 */
public class MatchPair {
    private Match match1=null;
    private Match match2=null;
    private long initTimestamp=-1;
    private long finishedTimestamp=-1;

    public long getInitTimestamp() {
        return initTimestamp;
    }

    public void setInitTimestamp(long initTimestamp) {
        this.initTimestamp = initTimestamp;
    }

    public long getFinishedTimestamp() {
        return finishedTimestamp;
    }

    public void setFinishedTimestamp(long finishedTimestamp) {
        this.finishedTimestamp = finishedTimestamp;
    }
    
    public boolean isFinished(){
        return this.match1!=null && this.match2!=null;
    }

    public Match getMatch1() {
        return match1;
    }

    public void setMatch1(Match match1) {
        this.match1 = match1;
    }

    public Match getMatch2() {
        return match2;
    }

    public void setMatch2(Match match2) {
        this.match2 = match2;
    }
    
    
}
