/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.classutils.rockpaperscissors;

/**
 *
 * @author lendle
 */
public class Match {
    private String user=null;
    private Choice choice=null;

    public Match() {
    }

    public Match(String user, Choice choice) {
        this.user=user;
        this.choice=choice;
    }
    
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Choice getChoice() {
        return choice;
    }

    public void setChoice(Choice choice) {
        this.choice = choice;
    }
    
}
