/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.classutils.rockpaperscissors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author lendle
 */
public class MatchPairs {

    private List<MatchPair> queue = new ArrayList<>();
    private static final int THRESHOLD = 500;

    public synchronized MatchPair submitMatch(Match match) {
        if (queue.size() > THRESHOLD) {
            List<MatchPair> newQueue = new ArrayList<>();
            for (MatchPair pair : queue) {
                if (pair.isFinished()) {
                    //purge old finished pairs
                    if((System.currentTimeMillis()-pair.getFinishedTimestamp())<=10*60*1000){
                        newQueue.add(pair);
                    }
                }else{
                    newQueue.add(pair);
                }
            }
            queue.clear();
            queue.addAll(newQueue);
        }
        for (MatchPair pair : queue) {
            if (pair.isFinished() == false) {
                pair.setMatch2(match);
                pair.setFinishedTimestamp(System.currentTimeMillis());
                return pair;
            }
        }
        MatchPair pair = new MatchPair();
        pair.setMatch1(match);
        pair.setInitTimestamp(System.currentTimeMillis());
        queue.add(pair);
        return pair;
    }

    public List<MatchPair> getFinishedMatchPairs(String user) {
        List<MatchPair> ret = new ArrayList<>();
        for (MatchPair matchPair : queue) {
            if (matchPair.isFinished()
                    && (user.equals(matchPair.getMatch1().getUser()) || user.equals(matchPair.getMatch2().getUser()))) {
                ret.add(matchPair);
            }
        }
        return ret;
    }

    public MatchPair getLastUnFinishedMatchPair(String user) {
        for (MatchPair matchPair : queue) {
            if (!matchPair.isFinished()
                    && ((matchPair.getMatch1() != null && user.equals(matchPair.getMatch1().getUser()))
                    || (matchPair.getMatch2() != null && user.equals(matchPair.getMatch2().getUser())))) {
                return matchPair;
            }
        }
        return null;
    }

    public MatchPair getLastFinishedMatchPair(String user) {
        List<MatchPair> pairs = this.getFinishedMatchPairs(user);
        Collections.sort(pairs, new Comparator<MatchPair>() {
            @Override
            public int compare(MatchPair o1, MatchPair o2) {
                return (int) (o2.getFinishedTimestamp() - o1.getFinishedTimestamp());
            }
        });
        return (pairs.size() > 0) ? pairs.get(0) : null;
    }

    public List<MatchPair> getFinishedMatchPairsNewerThan(String user, long timestamp) {
        List<MatchPair> ret = new ArrayList<>();
        List<MatchPair> pairs = this.getFinishedMatchPairs(user);
        for (MatchPair pair : pairs) {
            if (pair.getFinishedTimestamp() > timestamp) {
                ret.add(pair);
            }
        }
        return ret;
    }
}
