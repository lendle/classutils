/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imsofa.ilab.classutils.rockpaperscissors;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author lendle
 */
@WebServlet(name = "RockPaperScissorsGetLastResultServlet", urlPatterns = {"/rockpaperscissors/last"})
public class RockPaperScissorsGetLastResultServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.addHeader("Access-Control-Allow-Origin", "*");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            MatchPairs pairs=(MatchPairs) this.getServletContext().getAttribute(MatchPairs.class.getName());
            String user=request.getParameter("user");
            
            Map ret=new HashMap();
            MatchPair lastFinishedMatchPair=pairs.getLastFinishedMatchPair(user);
            MatchPair lastUnFinishedMatchPair=pairs.getLastUnFinishedMatchPair(user);
            if(lastFinishedMatchPair!=null){
                ret.put("lastFinishedMatchPair", lastFinishedMatchPair);
                Match you, enemy=null;
                if(lastFinishedMatchPair.getMatch1().getUser().equals(user)){
                    you=lastFinishedMatchPair.getMatch1();
                    enemy=lastFinishedMatchPair.getMatch2();
                }else{
                    you=lastFinishedMatchPair.getMatch2();
                    enemy=lastFinishedMatchPair.getMatch1();
                }
                ret.put("you", you);
                ret.put("enemy", enemy);
                boolean win=false;
                boolean lose=false;
                switch(you.getChoice()){
                    case ROCK:
                        if(enemy.getChoice().equals(Choice.PAPER)){
                            lose=true;
                        }else if(enemy.getChoice().equals(Choice.SCISSOR)){
                            win=true;
                        }
                        break;
                    case PAPER:
                        if(enemy.getChoice().equals(Choice.SCISSOR)){
                            lose=true;
                        }else if(enemy.getChoice().equals(Choice.ROCK)){
                            win=true;
                        }
                        break;
                    case SCISSOR:
                        if(enemy.getChoice().equals(Choice.ROCK)){
                            lose=true;
                        }else if(enemy.getChoice().equals(Choice.PAPER)){
                            win=true;
                        }
                        break;
                }
                ret.put("win", win);
                ret.put("lose", lose);
            }
            if(lastUnFinishedMatchPair!=null){
                ret.put("lastUnFinishedMatchPair", lastUnFinishedMatchPair);
            }
            out.print(new Gson().toJson(ret));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
