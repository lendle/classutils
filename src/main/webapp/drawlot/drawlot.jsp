<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.commons.csv.CSVFormat"%>
<%@page import="org.apache.commons.csv.CSVRecord"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>抽籤程式</title>
        <script
            src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
        crossorigin="anonymous"></script>
        <script>
            var studentList=null;
            $(document).ready(function () {
                $.ajax("../students?courseId=<%=request.getParameter("courseId")%>", {
                    success: function (data) {
                        studentList=data;
                        var table = $("#grid").get(0);
                        var cols=10;
                        var rows = Math.ceil((data.length / 1.0 - cols*2) / 2) + 2;
                        
                        for (var i = 0; i < rows; i++) {
                            var tr = document.createElement("tr");
                            table.appendChild(tr);
                            for (var j = 0; j < cols; j++) {
                                var td=null;
                                td=document.createElement("td");
                                if (!(i == 0 || i == rows - 1 || j == 0 || j == cols-1)) {
                                    $(td).css("border-width","0px");
                                    $(td).attr("enabled", "false");
                                }else{
                                    $(td).attr("enabled", "true");
                                }
                                var studentIndex=-1;
                                if(i==0){
                                    studentIndex=j;
                                }else if(i==rows-1){
                                    studentIndex=rows+cols-2+(cols-1-j);
                                }else if(j==cols-1){
                                    studentIndex=cols-1+i;
                                }else if(j==0){
                                    studentIndex=data.length-i;
                                }
                                if(studentIndex<data.length && $(td).attr("enabled")=="true"){
                                    $(td).attr("id", "grid"+(studentIndex));
                                    $(td).text(""+(data[studentIndex].name));
                                    $(td).attr("studentId", data[studentIndex].id);
                                    $(td).attr("studentName", data[studentIndex].name);
                                }
                                tr.appendChild(td);
                            }
                        }
                    }
                });
            });
            
            var currentGrid=-1;
            var currentTimeInterval=10;
            
            function step(goal){
                currentGrid++;
                if(currentGrid==studentList.length){
                    currentTimeInterval*=2;
                    currentGrid=0;
                }
                if(currentTimeInterval>100 && currentGrid==goal){
                    return;
                }
                $("td").css("background-color", "white");
                $("#grid"+currentGrid).css("background-color", "yellow");
                setTimeout(function(){
                    step(goal);
                }, currentTimeInterval);
            }
            
            function roll(){
                var index=Math.round(Math.random()*1000%studentList.length);
                currentGrid=-1;
                $("td").css("background-color", "white");
                currentTimeInterval=10;
                setTimeout(function(){
                    step(index);
                }, currentTimeInterval);
            }
        </script>
    </head>
    <body>
        <input type="button" onclick="roll()" value="roll"/><br/>
        <table border="1" style="width: 800px; height: 300px" id="grid">
            
        </table>
    </body>
</html>
