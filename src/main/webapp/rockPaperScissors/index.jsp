<%-- 
    Document   : index
    Created on : Oct 27, 2019, 9:57:39 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="joiningStage">
            User: <input type="text" id="userLogin"/><br/>
            <button onclick="join();">Join</button>
        </div>
        <div id="gamingStage" style="display: none">
            User: <input type="text" id="user" readonly="true"/><br/>
            Choice: <select id="choice">
                <option value="PAPER">Paper</option>
                <option value="ROCK">Rock</option>
                <option value="SCISSOR">Scissor</option>
            </select><br/>
            <input type="text" id="result" readonly="readonly"/><br/>
            <input id="submitButton" type="button" value="submit" onclick="submit();" disabled="true"/><br/>
            <table style="width: 100%;">
                <tr>
                    <td><div>You</div><img id="yours" src="rock_paper_larger.jpg"/></td>
                    <td><div id="enemy">Unknown</div><img id="others" src="rock_paper_larger.jpg"/></td>
                </tr>
            </table>
        </div>

        <script>
            var lastUnFinishedMatchPair = null;
            var lastFinishedMatchPair = null;
            function join() {
                $("#user").val($("#userLogin").val());
                $("#joiningStage").css("display", "none");
                $("#gamingStage").css("display", "block");
                $.ajax("../rockpaperscissors/last", {
                    data: {
                        user: $("#user").val()
                    }
                }).then((data) => {
                    console.log(data);
                    setTimeout(verifyGameState, 100);
                });
            }

            function verifyGameState() {
                $.ajax("../rockpaperscissors/last", {
                    data: {
                        user: $("#user").val()
                    }
                }).then((data) => {
                    console.log(data);
                    lastUnFinishedMatchPair = data.lastUnFinishedMatchPair;
                    lastFinishedMatchPair=data.lastFinishedMatchPair;
                    if (!data.lastUnFinishedMatchPair) {
                        $("#submitButton").removeAttr("disabled");
                        if(data.lastFinishedMatchPair){
                            $("#yours").attr("src", getCorrespondingImage(data.you));
                            $("#others").attr("src", getCorrespondingImage(data.enemy));
                            $("#enemy").text(data.enemy.user);
                            displayResult(data.win, data.lose);
                        }
                    }else{
                        $("#yours").attr("src", getCorrespondingImage(data.lastUnFinishedMatchPair.match1));
                        $("#others").attr("src", "rock_paper_larger.jpg");
                        $("#submitButton").attr("disabled", "disabled");
                    }
                    setTimeout(verifyGameState, 5000);
                });
            }
            
            function displayResult(win, lose){
                if(win){
                    $("#result").val("You Win!");
                }else if(lose){
                    $("#result").val("You Lose!");
                }else{
                    $("#result").val("DRAW!");
                }
            }
            
            function getCorrespondingImage(match){
                if(match.choice=="ROCK"){
                    return "rock.png";
                }else if(match.choice=="PAPER"){
                    return "paper.png";
                }else if(match.choice=="SCISSOR"){
                    return "scissors.png";
                }else{
                    return "rock_paper_larger.jpg";
                }
            }
            
            function submit(){
                $.ajax("../rockpaperscissors/submit", {
                    data: {
                        user: $("#user").val(),
                        choice: $("#choice").val()
                    }
                }).then((data) => {
                    console.log(data);
                    $("#result").val("");
                    $("#enemy").text("Unknown");
                    $("#submitButton").attr("disabled", "disabled");
                    $("#yours").attr("src", getCorrespondingImage(data.match1));
                    $("#others").attr("src", "rock_paper_larger.jpg");
                });
            }
        </script>
    </body>
</html>
